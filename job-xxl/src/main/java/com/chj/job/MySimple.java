package com.chj.job;

import com.chj.business.MyBusiness;
import com.enjoy.business.EnjoyBusiness;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 普通任务
 */
@JobHandler(value="mySimple") //value值对应的是调度中心新建任务的JobHandler
@Component
public class MySimple extends IJobHandler {

	@Autowired
	private MyBusiness myBusiness;

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		myBusiness.process(1,1,param);
		return SUCCESS;
	}

}
