package com.chj.job;

import com.chj.business.MyBusiness;
import com.cxytiandi.elasticjob.annotation.ElasticJobConf;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.springframework.beans.factory.annotation.Autowired;

@ElasticJobConf(name = "MySimpleJob",cron = "0/5 * * * * ?"
		,shardingItemParameters = "0=beijing|shenzhen|tianjin,1=shanghai",shardingTotalCount = 2
		,listener = "com.chj.handle.MessageElasticJobListener"
		,jobExceptionHandler = "com.chj.handle.CustomJobExceptionHandler"
)
public class MySimpleJob implements SimpleJob {
	@Autowired
	private MyBusiness myBusiness;
	public void execute(ShardingContext context) {
		System.out.println("MySimpleJob,当前分片："+context.getShardingParameter());
		//TODO 当前起始 context.getShardingParameter(),回返切片信息beijing
		String sql = myBusiness.getSql(context.getShardingParameter());
		myBusiness.process(sql);
	}
}