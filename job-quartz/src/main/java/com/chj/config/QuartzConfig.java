package com.chj.config;

import com.chj.job.BusinessJob;
import com.chj.job.XXXService;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.*;


@Configuration
public class QuartzConfig {
    /**
     * 耦合业务
     * TODO 将调度环境信息传递给业务方法，如：调度时间，批次等
     */
    @Bean(name = "businessJobDetail")
    public JobDetailFactoryBean businessJobDetail() {
        JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
        //业务bean是多例的还是单例的？ 多例
        jobDetailFactoryBean.setJobClass(BusinessJob.class);
        //TODO 将参数封装传递给job
        JobDataMap jobDataMap =new JobDataMap();
        jobDataMap.put("time",System.currentTimeMillis());
        jobDetailFactoryBean.setJobDataAsMap(jobDataMap);
        return  jobDetailFactoryBean;
    }
    /**
     * TODO 普通业务类
     */
    @Bean(name = "serviceBeanDetail")
    public MethodInvokingJobDetailFactoryBean serviceBeanDetail(XXXService serviceBean) {//业务bean，单例的
        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
        //TODO  是否并发执行
        jobDetail.setConcurrent(false);
        // TODO 需要执行的实体bean
        jobDetail.setTargetObject(serviceBean);
        // TODO 需要执行的方法
        jobDetail.setTargetMethod("business");
        return jobDetail;
    }
    //TODO 简单触发器
    @Bean(name = "simpleTrigger")
    public SimpleTriggerFactoryBean simpleTrigger(JobDetail businessJobDetail) {
        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
        trigger.setJobDetail(businessJobDetail);
        // TODO 设置任务启动延迟
        trigger.setStartDelay(0);
        //TODO  每5秒执行一次
        trigger.setRepeatInterval(3000);
        return trigger;
    }
    //TODO cron触发器
    @Bean(name = "cronTrigger")
    public CronTriggerFactoryBean cronTrigger(JobDetail serviceBeanDetail) {
        CronTriggerFactoryBean triggerFactoryBean = new CronTriggerFactoryBean();
        triggerFactoryBean.setJobDetail(serviceBeanDetail);
        triggerFactoryBean.setCronExpression("0/6 * * * * ?");
        return triggerFactoryBean;
    }

    //TODO 调度工厂,将所有的触发器引入
    @Bean(name = "scheduler")
    public SchedulerFactoryBean schedulerFactory(Trigger simpleTrigger, Trigger cronTrigger) {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        //TODO 延时启动，应用启动1秒后
        bean.setStartupDelay(1);
        //TODO 注册触发器
        bean.setTriggers(simpleTrigger,cronTrigger);
        return bean;
    }
}
